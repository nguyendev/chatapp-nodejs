const NodeCache = require("node-cache");

const cache = new NodeCache();

module.exports = (duration) => (req, res, next) => {
  if (req.method !== "GET") {
    console.log("cant cache");
    return next();
  }

  const key = req.originalUrl;
  const cacheRespose = cache.get(key);
  if (cacheRespose) {
    console.log(`cache hit for ${key}`);
    res.send(cacheRespose);
  } else {
    console.log(`Cache miss for key ${key}`);
    res.originalSend = res.send;
    res.send = (body) => {
        res.originalSend(body);
        cache.set(key, body, duration);
    }
    next()
  }
};
