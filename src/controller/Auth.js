const router = require("express").Router();
const jwt = require("jsonwebtoken");
const User = require("../model/User");
const bcrypt = require("bcrypt");
const validateEmail = require("../utils/checkValidateEmail");

//register
exports.register = async (req, res) => {    
  //generate new password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);
  //create new user
  const newUser = new User({
    userName: req.body.email.split("@")[0],
    email: req.body.email,
    password: hashedPassword,
  });
  try {
    //save user and respond
    if (validateEmail(req.body.email)) {
      const user = await newUser.save();
      res.status(200).json(user);
    }
    res.status(500).json({ msg: "unvalidate email" });
  } catch (err) {
    res.status(500).json({ msg: err });
  }
};

//login

exports.login = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    !user && res.status(404).json("user not found");

    const validPassword = await bcrypt.compare(
      req.body.password,
      user.password
    );
    !validPassword && res.status(400).json("wrong password");
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
      expiresIn: "1d",
    });
    const { _id, email, userName, profilePicture, followers, followings, saved } = user;
    res
      .status(200)
      .json({ token: token, user: { _id, email, userName, profilePicture, followers, followings, saved } });
  } catch (err) {
    res.status(500).json(err);
  }
};

exports.signout = (req, res) => {
  res.clearCookie("token");
  res.status(200).json({
    message: "Signout successfully...!",
  });
};
