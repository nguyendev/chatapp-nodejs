const mongoose = require("mongoose");
const Cmt = require("../model/Comment");
const Post = require("../model/Post");

class APIfeatures {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  paginating() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 9;
    const skip = (page - 1) * limit;
    this.query = this.query.skip(skip).limit(limit);
    return this;
  }
}

exports.createComment = async (req, res) => {
  const inputCmt = new Cmt({
    userId: req.body.userId,
    postId: req.body.postId,
    content: req.body.content,
    postUserId: req.body.postUserId,
  });

  try {
    const newCmt = await inputCmt.save();
    await Post.findOneAndUpdate(
      { _id: req.body.postId },
      {
        $push: { comments: newCmt._id },
      },
      { new: true }
    );
    res.status(200).json(newCmt);
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.getCommentAndAmout = async (req, res) => {
  try {
    const amountComment = await Cmt.find({
      postId: mongoose.Types.ObjectId(req.query.postId),
    }).count();
    const commentLast = await Cmt.find({
      postId: mongoose.Types.ObjectId(req.query.postId),
    })
      .populate("userId", "profilePicture userName followers")
      .populate("postId")
      .sort([["createdAt", -1]])
      .limit(1);
    res
      .status(200)
      .json({ amountCmt: amountComment, commentlast: commentLast });
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};
exports.getCommentsOfPost = async (req, res) => {
  try {
    const features = new APIfeatures(
      Cmt.find({
        postId: req.query.postId,
      }),
      { limit: req.query.limit, page: req.query.page }
    ).paginating();

    const cmts = await features.query
      .sort("-createdAt")
      .populate("userId", "profilePicture userName followers likes");

    res.json({
      msg: "Success!",
      result: cmts.length,
      cmts,
    });
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

// exports.deleteComment = async (req, res) => {
//   try {
//     const cmt = await Cmt.findById(req.params.id);
//     const post = await Post.findById(cmt.postId);
//     if (cmt.userId === req.body.userId || ) {
//       await cmt.deleteOne();
//       res.status(200).json("the post has been deleted");
//     } else {
//       res.status(403).json("you can't delete this post");
//     }
//   } catch (err) {
//     res.status(500).json(err);
//   }
// };
