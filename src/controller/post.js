const Post = require("../model/Post");
const User = require("../model/User");
const mongoose = require("mongoose");

class APIfeatures {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  paginating() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 9;
    const skip = (page - 1) * limit;
    this.query = this.query.skip(skip).limit(limit);
    return this;
  }
}

//create post
exports.createPost = async (req, res) => {
  const newPost = new Post({
    userId: mongoose.Types.ObjectId(req.body.userId),
    desc: req.body.desc,
    img: req.body.img,
  });

  try {
    const post = await newPost.save();
    return res.status(200).json(post);
  } catch (err) {
    return res.status(500).json("something went wrong");
  }
};

//update post

exports.updatePost = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (post.userId === req.body.userId) {
      await post.updateOne({ $set: req.body });
      res.status(200).json("the post has been updated");
    } else {
      res.status(403).json("you can update only your post");
    }
  } catch (err) {
    res.status(500).json(err);
  }
};

//delete post

exports.deletePost = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (post.userId === req.body.userId) {
      await post.deleteOne();
      res.status(200).json("the post has been deleted");
    } else {
      res.status(403).json("you can't delete this post");
    }
  } catch (err) {
    res.status(500).json(err);
  }
};

//like / dislike a post
exports.likeDislike = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (!post.likes.includes(req.body.userId)) {
      await post.updateOne({
        $push: { likes: mongoose.Types.ObjectId(req.body.userId) },
      });
      res.status(200).json("The post has been liked");
    } else {
      await post.updateOne({
        $pull: { likes: mongoose.Types.ObjectId(req.body.userId) },
      });
      res.status(200).json("The post has been disliked");
    }
  } catch (err) {
    res.status(500).json(err);
  }
};

//get a post
exports.getPost = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id).populate(
      "userId",
      "profilePicture userName followers likes"
    );
    res.status(200).json(post);
  } catch (err) {
    res.status(500).json(err);
  }
};

//get all your post

exports.getPostOfCurrentId = async (req, res) => {
  try {
    const userPosts = await Post.find({
      userId: mongoose.Types.ObjectId(req.query.userId),
    });
    res.json(userPosts);
  } catch (error) {
    res.status(500).json(error);
  }
};

//get posts
exports.getPosts = async (req, res) => {
  try {
    const currentUser = await User.findById(
      mongoose.Types.ObjectId(req.query.userId)
    );
    const features = new APIfeatures(
      Post.find({
        userId: [...currentUser.followings, req.user._id],
      }),
      { limit: req.query.limit, page: req.query.page }
    ).paginating();

    const posts = await features.query
      .sort("-createdAt")
      .populate("userId", "profilePicture userName followers likes")
      .populate({
        path: "comments",
        populate: {
          path: "userId",
          select: "-password",
        },
      });
    const data = {
      msg: "Success!",
      result: posts.length,
      posts,
    };
    res.json(data);
  } catch (err) {
    return res.status(500).json({ msg: err.message });
  }
};

//save post
exports.savePost = async (req, res) => {
  try {
    const user = await User.find({
      _id: mongoose.Types.ObjectId(req.query._id),
      saved: mongoose.Types.ObjectId(req.params.id),
    });
    if (user.length > 0) {
      const save = await User.findOneAndUpdate(
        { _id: req.user._id },
        {
          $pull: { saved: req.params.id },
        },
        { new: true }
      );

      if (!save)
        return res.status(400).json({ msg: "This user does not exist." });

      res.json({ msg: "unSaved Post!" });
    } else {
      const save = await User.findOneAndUpdate(
        { _id: req.user._id },
        {
          $push: { saved: req.params.id },
        },
        { new: true }
      );

      if (!save)
        return res.status(400).json({ msg: "This user does not exist." });

      res.json({ msg: "Saved Post!" });
    }
  } catch (err) {
    return res.status(500).json({ msg: err.message });
  }
};
