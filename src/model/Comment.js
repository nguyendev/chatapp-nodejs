const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "User"
    },
    postId: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Post"
    },
    content: {
      type: String,
      max: 500,
      required: true,
    },
    postUserId: mongoose.Types.ObjectId,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Comment", CommentSchema);
