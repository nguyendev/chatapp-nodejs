const express = require("express");
const { login, signout, register } = require("../controller/Auth");

const router = express.Router();

router.post("/signin", login);
router.post("/signout", signout);
router.post("/register", register);

module.exports = router;
