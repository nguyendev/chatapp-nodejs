const express = require("express");
const {
  updateUser,
  followUser,
  unfollow,
  findAUser,
  getUser,
} = require("../controller/User");
const { requireSignin } = require("../common-middleware/require-signin");

const router = express.Router();

router.put("/:id/unfollow", requireSignin, unfollow);
router.put("/:id/follow", requireSignin, followUser);
router.put("/:id", requireSignin, updateUser);
router.get("/find/user", requireSignin, findAUser);
router.get("/get/user", requireSignin, getUser);

module.exports = router;
