const express = require("express");
const {
  createPost,
  updatePost,
  deletePost,
  likeDislike,
  getPost,
  getPostOnFeed,
  getPostOfCurrentId,
  getPosts,
  savePost,
} = require("../controller/post");
const { requireSignin } = require("../common-middleware/require-signin");
const cache = require("../common-middleware/routeCache");
const router = express.Router();

router.post("/", requireSignin, createPost);
router.put("/:id"), requireSignin, updatePost;
router.delete("/:id", requireSignin, deletePost);
router.put("/:id/like", requireSignin, likeDislike);
router.get("/:id", requireSignin, cache(300), getPost);
router.get("/get/post/on/feed", requireSignin, cache(300), getPosts);
router.get("/own/post", requireSignin, getPostOfCurrentId);
router.put("/:id/save", requireSignin, savePost);

module.exports = router;
