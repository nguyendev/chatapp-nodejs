const express = require("express");
const {
  createComment,
  getCommentAndAmout,
  getCommentsOfPost,
} = require("../controller/comment");
const { requireSignin } = require("../common-middleware/require-signin");
const cache = require("../common-middleware/routeCache");
const router = express.Router();

router.post("/createComment", requireSignin, createComment);
router.get("/getLastComment", requireSignin, getCommentAndAmout);
router.get("/getCommentOfPost", requireSignin, cache(300), getCommentsOfPost);

module.exports = router;
